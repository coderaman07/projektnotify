import Head from "next/head";
import Footer from "../components/footer";
import MainPage from "../components/mainPage";

export default function Home({ Data, githubData, PostURL }) {
  let curDate = new Date();
  let month;
  if (curDate.getMonth() < 10) {
    month = `0${curDate.getMonth()}`
  } else {
    month = `${curDate.getMonth()}`
  }
  return (
    <>
      <Head>
        <title>{`${Data.projectName} | Projekt by ${githubData.name}`}</title>
        <meta name="description" content={Data.Description} />
        <meta name="keywords" content={Data.keywords} />
        <meta name="robots" content="index, follow"/>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <meta
            property="og:title"
          content={`${Data.projectName} | Projekt by ${githubData.name}`}
          />
          <meta
            property="og:site_name"
          content={`${Data.projectName} | Projekt by ${githubData.name}`}
          />
          <meta property="og:type" content="article" />
          <meta
            property="og:url"
            content={"https://projektnotify.vercel.app"}
          />
          <meta
            property="og:image"
            content={'https://codeitdown.vercel.app/'}
          />
          <meta
            property="og:description"
          content={Data.Description}
          />
          <meta
            name="twitter:card"
            content="summary_large_image"
          />
          <meta
            name="twitter:title"
          content={`${Data.projectName} | Projekt by ${githubData.name}`}
          />
          <meta
            name="twitter:description"
          content={Data.Description}
          />
          <meta
            name="twitter:image"
            content={'https://projektnotify.vercel.app'}
          />
        <meta property="og:site_name" content={`${Data.projectName} | Projekt by ${githubData.name}`} />
          <meta name="twitter:site" content="https://twitter.com/coderaman7" />
          <meta property="article:published_time" content={`${curDate.getFullYear()}-${month}-${curDate.getDate()}T00:00:00.000Z`} />
          <meta property="article:modified_time" content={`${curDate.getFullYear()}-${month}-${curDate.getDate()}T00:00:00.000Z`} />
      </Head>
      <MainPage title={Data.projectName} PostBody={Data.Description} POSTURL={PostURL} />
      <Footer githubData={githubData} />
    </>
  )
}

export const getStaticProps = async (context) => {
  let dev = process.env.NODE_ENV !== 'production';
  const server = dev ? "http://localhost:3000/" : "https://projektnotify.vercel.app/"
  let Data = await (await fetch(process.env.GET_Request)).json()
  let githubData = await (await fetch(process.env.GITHUB)).json()
  let PostURL = await (await fetch(`${server}/api/hello`)).json()
  return {
    props: {
      Data,
      githubData,
      PostURL
    },
    revalidate: 1,
  }
}
