import { Router } from "next/router";
import { useState } from "react";

export default function DeletePage(){

    const [Email, setEmail] = useState("");

    return(
        <>
            <form style={{ marginTop: `20%` }} action={`https://projektnotify.pythonanywhere.com/remove/${Email}`} method="post">
                <table className="table text-xl mx-auto">
                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="email" className="font-bold px-4">
                                    Email
                                </label>
                            </td>
                            <td>
                                <input type="email" name="email" required="" onChange={(e) => { setEmail(e.target.value) }} autoFocus="" className="flex-1 block w-full border-2 border-gray-300 rounded focus:ring-indigo-500 focus:border-indigo-500" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="text-center mt-5 sm:mt-8 sm:flex justify-center">
                    <div className="rounded-md shadow">
                        <button type="submit" className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10" >
                            Un-Subscribe me!
                        </button>
                    </div>
                </div>
            </form>
        </>
    )
}