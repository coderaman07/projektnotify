export default function DeletedBackendData({data}) {
    return (
        <>
            <div className="text-center mt-5 sm:mt-8 sm:flex justify-center">
                <div className="rounded-md shadow">
                    <p className="mx-auto p-3 text-xl text-center border-2 border-green-300 rounded bg-green-200 text-green-700 sm:max-w-xl">
                        {data}.
                    </p>
                </div>
            </div>
        </>
    )
}

export const getStaticProps = async (context) => {
    return {
        props: {
            data : context.params.slug
        },
        revalidate: 1,
    }
}

export const getStaticPaths = async () => {
    return {
        paths: [],
        fallback: true
    }
}