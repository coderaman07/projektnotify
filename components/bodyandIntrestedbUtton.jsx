import Link from "next/link";
import ReactMarkdown from "react-markdown";


export default function ButtonAndPost({PostBody, setclicked}){

    return(
        <>
            <ReactMarkdown className="linebreak">{PostBody}</ReactMarkdown>
            <div className="mt-5 sm:mt-8 sm:flex justify-center">
                <div className="rounded-md shadow" style={{ marginTop: "2em" }}>
                    <a onClick={() => setclicked(true) } className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10" style={{ cursor: "pointer" }}>
                        I&apos;m interested!
                    </a>
                </div>
            </div>
            </>
    )
}