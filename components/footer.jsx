export default function Footer({githubData}){
    return(
        <footer
            className="
  mt-10
  text-center
  mx-auto
  max-w-7xl
  px-4
  sm:mt-12
  sm:px-6
  md:mt-16
  lg:mt-20
  lg:px-8
"
        >
            <p className="text-sm">
                A product by <a className="text-indigo-600" href={githubData.blog}> {githubData.name}</a>.
            </p>
        </footer>

    )
}