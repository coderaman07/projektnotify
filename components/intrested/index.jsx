import { useState } from "react";
import handler from "../../pages/api/hello";
import AfterSubmit from "../submitted";

export default function FormForm({ setClicked, POSTURL }) {

    const [email, setEmail] = useState("");
    const [submitted, setSubmitted] = useState(false);
    console.log(POSTURL)

    async function handleSubmit (e){
        e.preventDefault();
        var formdata = new FormData();
        formdata.append("email", email);

        var requestOptions = {
            method: 'POST',
            body: formdata,
            redirect: 'follow'
        };

        let datafeatch = await fetch(POSTURL.postReq, requestOptions)
        if ( datafeatch.status == 202 ){
            setSubmitted(true);
        }
    }

    return (
        <>
            {submitted ? <AfterSubmit /> : <form onSubmit={(e) => handleSubmit(e)} method="post">
                <table className="table text-xl mx-auto">
                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="email" className="font-bold px-4">
                                    Email
                                </label>
                            </td>
                            <td>
                                <input type="email" name="email" required="" onChange={(e) => { setEmail(e.target.value) }} autoFocus="" className="flex-1 block w-full border-2 border-gray-300 rounded focus:ring-indigo-500 focus:border-indigo-500" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="text-center mt-5 sm:mt-8 sm:flex justify-center">
                    <div className="rounded-md shadow">
                        <button type="submit" className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10" >
                            Let me know!
                        </button>
                    </div>
                </div>
            </form>}
            <div className="text-center mt-5 sm:mt-8 sm:flex justify-center">
                <div className="rounded-md shadow">
                    <button onClick={()=>setClicked(false)} className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10" >
                        Take me Back!
                    </button>
                </div>
            </div>
        </>
    )
}