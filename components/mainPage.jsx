import { useState } from "react";
import ButtonAndPost from "./bodyandIntrestedbUtton";
import FormForm from "./intrested";


export default function MainPage({title, PostBody, POSTURL}){
    const [clicked, setClicked] = useState(false);
    return(
        <main className="mt-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28 mobil">
            <div className="text-center">
                <h1 className="text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl" style={{paddingBottom: "3vh"}}>
                    <span className="block xl:inline">{title}</span>
                </h1>
                {clicked ? <p className="m-3 md:mt-5 mx-auto text-base text-gray-500 sm:text-lg sm:max-w-xl md:text-xl">
                    Fill in your email address and I&apos;ll let you know when there&apos;s something to
                    show.
                </p> : ""}
                {clicked ? <FormForm setClicked={setClicked} POSTURL={POSTURL} /> : <ButtonAndPost PostBody={PostBody} setclicked={setClicked} />}
            </div>
        </main>
    )
}