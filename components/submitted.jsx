export default function AfterSubmit(){
    return(
        <div className="text-center mt-5 sm:mt-8 sm:flex justify-center">
            <div className="rounded-md shadow">
                <p className="mx-auto p-3 text-xl text-center border-2 border-green-300 rounded bg-green-200 text-green-700 sm:max-w-xl">
                    Thank you for submitting your interest.
                </p>
            </div>
        </div>

    )
}