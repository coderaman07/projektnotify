import { useEffect } from "react";
import Router from 'next/router'

export default function DeletedBackendData({ data }) {
    useEffect(() => {
        Router.push(`https://projektnotify.herokuapp.com/remove/${data}/`)
    }, []);
}

export const getStaticProps = async (context) => {
    return {
        props: {
            data: context.params.slug
        },
        revalidate: 1,
    }
}

export const getStaticPaths = async () => {
    return {
        paths: [],
        fallback: true
    }
}